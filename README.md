# mario

Java MVC框架实现教程源码

https://gitee.com/eric-tutorial/java-bible/blob/master/mvc/index.md


###  代码行数

![](代码行数.png)


### 安装到本地

安装到本地m2仓库，供mario-sample调用

```shell script
mvn clean install
```


使用案例 https://gitee.com/eric-tutorial/mario-sample
